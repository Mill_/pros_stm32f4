# PROS on stm32f4
## Esempi
* [Appunti sui pin mode](00_pin_mode)
* [Two buttons - two interrupts](01_two_buttons_two_ints)
* [Appunti ed esempi su UART e USART](02_UART_USART)

## Guidelines (Code and other stuff)

### Interrupt handler
Gli interrupt handler possono essere scritti in due modi.
Nel primo caso, il programmatore si prende il carico della gestione di tutto mentre, nel secondo caso, delega la prima parte di gestione dell'interrupt all'HAL e scrive solo la routine da eseguire in base al segnale arrivato.

Prima cosa che fa la routine di risposta è quella di azzerare l'interrupt alla sorgente.
(Clear della periferica -> abbassa interrupt sulla periferica.)

Di seguito le due modalità di gestione descritte precedentemente:


- Tutto scaricato sul programmatore
```
if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_X) != RESET)
{
    // Interrupt clean
    __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_X);

    ...
    /* Interrupt routine */
    ...
}
```

- La parte della gestione dell'interrupt scaritata all'HAL (clean dell'interrupt).
```
// Delega l'HAL per fare il clean dell'interrupt
void EXTI0_IRQHandler(void){
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_X);
}


// sovrascrizione di '__weak void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)' presente in stm32f4xx_hal_msp.c
// richiamata dalla funzione HAL_GPIO_EXTI_IRQHandler(uint16_t GPIO_Pin)
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
    if(GPIO_Pin == GPIO_PIN_X)
    {
        HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_14); // LED 5 - Red
        for(int i=0; i<1000000; i++);
    }
}
```

### Fritzing files
Recuperato dal seguente thread: [STM32407VG Discovery Fritzing](https://forum.fritzing.org/t/stm32407vg-discovery-fritzing-folder/9310)\
oppure download diretto [STM32407VG-Discovery.fzpz](https://forum.fritzing.org/uploads/short-url/og05EiTkK6QyTv1Jr8GnSEqJfuH.fzpz)
