#!/usr/bin/bash

usage(){
    echo -ne "\nusage:"
    echo -ne "\t${0##*/}" # script name
    echo -e " PROJECT_NAME [NEW_PROJECT_NAME]\n"
}

if [ $# -lt 1 ]
then
    usage
    exit 0
fi

PROJECT_NAME=$1

PROJECT_NAME="${PROJECT_NAME///}" # Remove final '/' from the name

rm -irf $PROJECT_NAME/Debug/
rm -if $PROJECT_NAME/*Debug.launch
rm -if $PROJECT_NAME/.mxproject

if [ $# -eq 2 ]
then
    CHNG_NAME=$2
    CHNG_NAME="${CHNG_NAME///}" # Remove final '/' from the name

    find $PROJECT_NAME -type f -print0 | xargs -0 sed -i 's/'$PROJECT_NAME'/'$CHNG_NAME'/g'
    mv $PROJECT_NAME/$PROJECT_NAME'.ioc' $PROJECT_NAME/$CHNG_NAME'.ioc'
    mv $PROJECT_NAME $CHNG_NAME
fi
