# UART e USART

## Tool utilizzati
* Serial Port Terminal (es. [gtkterm](https://linux.die.net/man/1/gtkterm), [microcom](https://manpages.ubuntu.com/manpages/bionic/man1/microcom.1.html), [tio](https://tio.github.io/), etc)
* [FTDI dongle](https://en.wikipedia.org/wiki/FTDI)

## Prerequisiti e Troubleshooting

### Dongle echo test
Per controllare se il dongle che si possiede è del tutto
funzionante, la prima cosa da fare è provare a eseguire una funzione di `echo`.

Per prima cosa, bisogna collegare il pin di trasmissione(TX) a quello di ricezione(RX), come mostrato nella figura.

![alt text](src/FTDI_echo.png "FTDI echo")

Fritzing file: [FTDI\_echo.fzz](src/FTDI_echo.fzz)


Una volta che si è collegato al pc tramite USB, utilizzando uno dei terminali citati prima, si può
fare il test: se sul terminale appare il carattere inserito da tastiera, il test ha avuto successo.

Per questo test, è stato usato il setup di default del serial terminal, come mostrato nell'immagine.

![alt text](src/gtkterm_setup.png "FTDI echo")


E questo è il risultato finale del test.

![alt text](src/gtkterm_hello.png "FTDI echo")


**P.S**  
Può succedere che si veda echo doppio; in quel caso è il sw del terminale che fa anche echo locale, di default di solito però non succede

### Collegamenti TX-RX
Una volta sicuri che il dongle sia perfettamente funzionante, è il momento di andare a collegarlo
col ala board che si vuole usare per la comunicazione via seriale.
I pin dovranno essere collegati, in base a com'è costruito il circuito, o RX-RX e TX-TX oppure RX-TX e TX-RX.
Di seguito ci sono i link che spiegano le motivazioni.
* [RS232 DTE versus DCE](https://zone.ni.com/reference/en-XX/help/370984T-01/lvaddon11/987x_rs232dtevsdce/)
* [Difference Between DTE and DCE](https://techdifferences.com/difference-between-dte-and-dce.html)
* [Difference Between DTE and DCE, 2](https://www.geeksforgeeks.org/difference-between-dte-and-dce/)

**P.S**  
Se non funziona in un modo, provate a cambiare come avete collegato i pin e dovrebbe funzionare tutto (tipo USB)
![alt text](src/usb_wrong.jpg "usb")

### Other things
* Cavo [Null modem](https://en.wikipedia.org/wiki/Null_modem) per connettere due DTE fra loro, incrociando TX e RX 
