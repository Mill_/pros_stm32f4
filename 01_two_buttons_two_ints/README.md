# Two buttons -> two interrupts with different priority

## Example code
### File modificati dopo la generazione del codice tramite IDE
* [main.c](code/main.c)
* [stm32f4xx\_hal\_msp.c](code/stm32f4xx_hal_msp.c)

I seguenti file sono situati sotto il seguente path del progetto: `/Code/Src`.
### Approccio lazy
* [progetto](code/externalButtonSyscall.zip)

## Pinout & Configuration
Confugurazione dei pin per il seguente esempio.
![alt text](src/pinout_conf.png "configuration")

Di seguito due tra gli approcci possibili per il setting del bottone esterno: pulldown e pullup.

## PULLDOWN
### Electric schema
![alt text](src/pulldown/external_button_pulldown.png "Electric schema - pulldown")
Fritzing file: [external\_button\_pulldown.fzz](src/pulldown/external_button_pulldown.fzz) 

### Code
```
/*Configure GPIO pin : PC1 */
GPIO_InitStruct.Pin = GPIO_PIN_1;
//  PULLDOWN
GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
GPIO_InitStruct.Pull = GPIO_PULLDOWN;
HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

```

## PULLUP
### Electric schema
![alt text](src/pullup/external_button_pullup.png "Electric schema - pullup")
Fritzing file: [external\_button\_pullup.fzz](src/pullup/external_button_pullup.fzz) 

### Code
```
/*Configure GPIO pin : PC1 */
GPIO_InitStruct.Pin = GPIO_PIN_1;
// PULLUP
GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
GPIO_InitStruct.Pull = GPIO_PULLUP;
HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
```

## Enable IRQ
Nella parte finale del metodo `MX_GPIO_Init` aggiungere il seguente snippet di codice per abilitare le due linee di interrupt per i due pulsanti.
```
/* Enable IRQ */
HAL_NVIC_SetPriority(EXTI0_IRQn, 1, 0);
HAL_NVIC_EnableIRQ(EXTI0_IRQn);

HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
HAL_NVIC_EnableIRQ(EXTI1_IRQn);
```
Come da documentazione, il metodo `HAL_NVIC_SetPriority(IRQn, PreemptPriority, SubPriority)`
configura la priorità di uno specifico interrupt mentre `HAL_NVIC_EnableIRQ(IRQn_Type IRQn)` lo abilita a livello di NVIC.

## Interrupt priority
Si può abilitare la priorità in due modi: manuale o tramite l'IDE (STM32CubeIDE).

### Via IDE configuration
Con l'IDE è molto semplice, basta aprire la schermata di "pinout view" e cambiare le impostazioni dal pannello di "System Core".
I vari passaggi sono i seguenti:
* Aprire "Pinout & Configuration"
* Dal menu accanto, sotto "System Core", selezioonare NVIC
* Se non si vede la voce "Priority Group" (come da immagine), allargare la finestra (il resize risolverà il problema)
* selezionare il corretto "Priority Group", in questo caso, basta 1
* una volta che si è selezionato il "Priority Group", si può assegnare il gruppo alle varie linee di interrupt abilitate

![alt text](src/priority_group_ide.png "Priority Group - settings")


### Via config file
Come visto precedentemente, nella sezione "Enable IRQ", i due interrupt sono abilitati con le diverse priorità.
Sfortunatamente l'IDE nasconde alcuni parametri gestiti nel codice, uno in particolare è il "Priority Group"; bisogna ricordarsi di cambiarlo in base alle proprie necessità.
Di default, il suo valore è 0, quindi, nel caso in cui non ci si ricordasse di andare a cambiarlo a 1, si riscontrerebbero delle problematiche durante l'esecuzione del programma.
Per modificare manualmente questo paramentro, bisogna aprire il file "stm32f4xx\_hal\_msp.c" sotto la directory "/Core/Src/" del progetto e andare a modificare il valore delle seguenti linee di codice.
```
void HAL_MspInit(void)
{
  /* USER CODE BEGIN MspInit 0 */

  /* USER CODE END MspInit 0 */

  __HAL_RCC_SYSCFG_CLK_ENABLE();
  __HAL_RCC_PWR_CLK_ENABLE();

  HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_1); // Priority Group 1

  /* System interrupt init*/

  /* USER CODE BEGIN MspInit 1 */

  /* USER CODE END MspInit 1 */
}
```
