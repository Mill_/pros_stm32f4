# Appunti sui pin mode

Come descritto nel Cheat Sheet della famiglia delle board STM32F4, ogni pin ha
internamente la circuiteria per gestire l'input come `PULLDOWN` e `PULLUP`; cioè
viene attivata una resistenza interna o verso massa (`PULLDOWN`) o verso alimentazione (`PULLUP`).
Per il circuito esterno vengono utilizzate delle resistenze alte di valore 10/20kΩ.

Di seguito verranno descritti i diversi in cui si può configurare un determinato PIN.
In base alla specifica configurazione del PIN verranno associati gli schemi
elettrici utilizzati per ricreare la situazione descritta.
L'esempio di base sarà quello di utilizzare il PIN per gestire un pulsante esterno
alla board.

## Configurazioni
Di seguito si parlerà di RISING e FALLING; la differenza tra i due è quando viene attivato l'interrupt associato al PIN.
* **RISING** - parte quando il segnale passa da 0 a 1
* **FALLING** - parte quando il segnale passa da 1 a 0

Quando si configura il PIN a `NOPULL`, significa che non vogliamo che la board
utilizzi alcun circuito interno per inizializzare il PIN; quindi si dovrà
creare il circuito esternamente.
In questo caso abbiamo 4 modi differenti di configurare il PIN:
* **GPIO\_MODE\_IT\_RISING** **:** **GPIO_NOPULL** - con circuito esterno di `PULLDOWN`, il led si accende quando si preme il pulsante
* **GPIO\_MODE\_IT\_FALLING** **:** **GPIO_NOPULL** - con circuito esterno di `PULLDOWN`, il led si accende quando si rilascia il pulsante
* **GPIO\_MODE\_IT\_RISING** **:** **GPIO_NOPULL** - con circuito esterno di `PULLUP`, il led si accende quando si preme il pulsante
* **GPIO\_MODE\_IT\_FALLING** **:** **GPIO_NOPULL** - con circuito esterno di `PULLUP`, il led si accende quando si rilascia il pulsante

Questo significa che le 4 configurazioni descritte prima possono essere ridotte
a 2 esempi significativi:
* **GPIO\_MODE\_IT\_RISING** **:** **GPIO_NOPULL** - con circuito esterno di `PULLDOWN`
* **GPIO\_MODE\_IT\_FALLING** **:** **GPIO_NOPULL** - con circuito esterno di `PULLUP` 

Ovviamente, lo schema di configurazioni appena descritto è quello che si può
fare mettendo `GPIO_PULLDOWN` o `GPIO_PULLUP`, senza circuiti esterni di `PULLUP` o `PULLDOWN`.


## GPIO\_NOPULL

Il codice seguente è usato per accendere un led quando il pulsante viene premuto, con la configurazione di `PULLDOWN` esterno.

#### Code
```
/*Configure GPIO pin : PC1 */
GPIO_InitStruct.Pin = GPIO_PIN_1;
GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
GPIO_InitStruct.Pull = GPIO_NOPULL;
HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
```
Lo schema elettrico di `PULLDOWN`, associato al codice, è il seguente:

![alt text](src/external_button_pulldown.png "Electric schema - pulldown")
Fritzing file: [external\_button\_pulldown.fzz](src/external_button_pulldown.fzz)



Il codice seguente è usato per accendere un led quando il pulsante viene premuto, con la configurazione di `PULLUP` esterno.

#### Code
```
/*Configure GPIO pin : PC1 */
GPIO_InitStruct.Pin = GPIO_PIN_1;
GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
GPIO_InitStruct.Pull = GPIO_NOPULL;
HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
```
Lo schema elettrico di `PULLUP`, associato al codice, è il seguente:

![alt text](src/external_button_pullup.png "Electric schema - pullup")
Fritzing file: [external\_button\_pullup.fzz](src/external_button_pullup.fzz)


## GPIO\_PULLDOWN

Il codice seguente è usato per accendere un led quando il pulsante viene premuto, con la configurazione di `PULLDOWN` interna, quindi senza resistenza esterna.

#### Code
```
/*Configure GPIO pin : PC1 */
GPIO_InitStruct.Pin = GPIO_PIN_1;
GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
GPIO_InitStruct.Pull = GPIO_PULLDOWN;
HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
```

In questo caso, come spiegato in precedenza, si sta istruendo la board a utilizzare i circuiti
al suo interno per creare uno schema di tipo `PULLDOWN`; per questo motivo, non serve aggiungere la parte di circuiteria per creare quel circuito.
Come si vede nell'immagine successiva, basta aggiungere il circuito del bottone alla board per far funzionare il sistema

![alt text](src/external_button_pulldown_rint.png "Electric schema - internal pulldown")
Fritzing file: [external\_button\_pulldown\_rint.fzz](src/external_button_pulldown_rint.fzz)

## GPIO\_PULLUP

Il codice seguente è usato per accendere un led quando il pulsante viene premuto, con la configurazione di `PULLUP` interna, quindi senza resistenza esterna.

#### Code
```
/*Configure GPIO pin : PC1 */
GPIO_InitStruct.Pin = GPIO_PIN_1;
GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
GPIO_InitStruct.Pull = GPIO_PULLUP;
HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
```

In questo caso, come spiegato in precedenza, si sta istruendo la board a utilizzare i circuiti
al suo interno per creare uno schema di tipo `PULLUP`; per questo motivo, non serve aggiungere la parte di circuiteria per creare quel circuito.
Come si vede nell'immagine successiva, basta aggiungere il circuito del bottone alla board per far funzionare il sistema

![alt text](src/external_button_pullup_rint.png "Electric schema - internal pullup")
Fritzing file: [external\_button\_pullup\_rint.fzz](src/external_button_pullup_rint.fzz)
